import os
import cv2 as cv

in_folders = ['./Data/rock/', './Data/scissors/', './Data/paper/']
out_folders = ['./Data/rock_segmenation/', './Data/scissors_segmenation/', './Data/paper_segmenation/']

def rps_segmenations(in_folders, out_folders, display=True):
    """Generate segmenation masks for images in dataset.
    """
    for in_dir, out_dir in zip(in_folders, out_folders):
        files = os.listdir(in_dir)
        for fn in [f for f in files if f.endswith('.png')]:
            img = cv.imread(in_dir + fn)
            img = cv.resize(img, (0, 0), fx=0.2, fy=0.2)
            img = cv.cvtColor(img, cv.COLOR_BGR2HSV)
            mask = cv.inRange(img, (45, 0, 0), (75, 255, 255))
            mask = cv.bitwise_not(mask)
            if display:
                img = cv.bitwise_not(img, img, mask=mask)
                img = cv.cvtColor(img, cv.COLOR_HSV2BGR)
                cv.imshow('img', img)
                cv.imshow('mask', mask)
                cv.waitKey()
            else:
                cv.imwrite(out_dir + fn, mask)

if __name__ == "__main__":
    rps_segmenations(in_folders, out_folders, False)

