# RPS Classifier

The RPS Classifier predicts who is going to win a battle of rock-paper-scissors. An image consisting of two RPS-gestures of people combating each other is given as input. To simplify the training process, the network splits in two branches, which are joined again later. This way, instead of letting the network learn both the gestures and rules of rock-paper-scissors out of the blue, the task of appointing a winner is divided in three parts. First, each branch tries to classify one of the gestures (*lo* and *ro*). Next, these classification results are used to train the classifier of which person wins the battle (*wo*). The network makes use of the Adadelta optimizer, which allows for a decreasing learning rate.



![Network architecture](./Output/network.png)



Early stopping is configured on the validation loss for the classifier predicting the winner with a patience of 10 epochs. An additional callback makes sure the the best performing model (model of epoch achieving lowest winner loss) is saved. Plots of the loss and accuracy are drawn up.



## Dataset

The used dataset contains RPS-gestures captured in front of a green screen. Chroma keying is used to segment the gestures. Each sample for training consists of two randomly selected gestures that are joined together with one of the two gestures mirrored to let it look like two people are having a RPS battle against each other. 

The dataset is split in a train, a validation and a test set (see `./Data/*.txt`). The network is trained using the train set, with the validation set for validation.

See [https://www.kaggle.com/drgfreeman/rockpaperscissors](https://www.kaggle.com/drgfreeman/rockpaperscissors) to obtain the dataset used for generating the training data, constructed by Julien de la Bruère-Terreault. 

> This dataset contains a total of 2188 images corresponding to the 'Rock' (726 images), 'Paper' (710 images) and 'Scissors' (752 images) hand gestures of the Rock-Paper-Scissors game. All image are taken on a green background with relatively consistent lighting and white balance.
>
> All images are RGB images of 300 pixels wide by 200 pixels high in .png format. The images are separated in three sub-folders named 'rock', 'paper' and 'scissors' according to their respective class.



## To Do

- [x] Generate masks for dataset images
- [x] Classification network to predict winner
- [ ] Segmentation network (U-Net) to segment hands in live-feed
- [ ] Live test environment



## Structure

| Path                       | Description                                                  |
| -------------------------- | ------------------------------------------------------------ |
| ./generate_segmenations.py | Script to generate the segmentations masks for the dataset (chroma keying) |
| ./train_predictor.py       | Script to train a predictor                                  |
| ./Output/                  | Folder containing plot of network architecture, loss and accuracy |
| ./Data/                    | Folder containing structure for dataset and files for the train-validation-test split. |

