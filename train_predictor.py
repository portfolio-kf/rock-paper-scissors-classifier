import math
import numpy as np
import tensorflow as tf
import cv2 as cv
import matplotlib.pyplot as plt

EPOCHS = 50
BATCH_SIZE = 32

MODEL = None
MIN_VAL_LOSS = float('inf')

class DataSequence(tf.keras.utils.Sequence):
    """Using data sequences for training (avoiding overfitting).
    Two random images from the dataset are stitched together to simulate a RPS-battle.
    """

    def __init__(self, train_data, batch_size):    
        """Initialize sequence generator."""
        self.train_data = train_data
        self.batch_size = batch_size
        self.indices = np.random.permutation(len(self.train_data[0]))
   
    def __len__(self):
        return math.floor(len(self.train_data[0]) / self.batch_size)
 
    def on_epoch_end(self):
        """Re-permutate the IDs (combating overfitting)."""
        self.indices = np.random.permutation(len(self.train_data[0]))
    
    def __getitem__(self, idx):
        """Get a case to train with. Two random images are stiched together, and the expressed gestures as well as the result of the battle is added as label."""
        idx_a = idx
        imgs_a, labels_a = self.get_one_side(idx_a)
        
        idx_b = idx_a
        while (idx_b == idx_a): idx_b = np.random.randint(0, self.__len__())
        imgs_b, labels_b = self.get_one_side(idx_b)
        
        imgs_a = [cv.flip(img, 1) for img in imgs_a]
        imgs = np.concatenate([imgs_a, imgs_b], axis=2)

        x = {
            'in': np.array(imgs)
        }
        y = {
            'lo': np.array(self.rps_to_bin(labels_a)),
            'ro': np.array(self.rps_to_bin(labels_b)),
            'wo': np.array(self.decide_winner(labels_a, labels_b)),
        }

        return x, y

    def get_one_side(self, idx):
        """Get image of one side of the battle."""
        imgs = self.train_data[0][self.indices[idx * self.batch_size:(idx + 1) * self.batch_size]]
        labels = self.train_data[1][self.indices[idx * self.batch_size:(idx + 1) * self.batch_size]]
        return imgs, labels

    def rps_to_bin(self, rps):
        """Convert the label of a gesture to a binairy notation."""
        labels = []
        for a in rps:
            if a == 'r':
                labels.append(np.array([[1], [0], [0]]))
            elif a == 'p':
                labels.append(np.array([[0], [1], [0]]))
            elif a == 's':
                labels.append(np.array([[0], [0], [1]]))
        return labels

    def decide_winner(self, labels_a, labels_b):
        """Calculate winner label."""
        labels = []
        for a, b in zip(labels_a, labels_b):
            if a == 'r':
                if b == 'r':
                    labels.append(np.array([[0], [1], [0]]))
                elif b == 'p':
                    labels.append(np.array([[0], [0], [1]]))
                elif b == 's':
                    labels.append(np.array([[1], [0], [0]]))
            elif a == 'p':
                if b == 'r':
                    labels.append(np.array([[1], [0], [0]]))
                elif b == 'p':
                    labels.append(np.array([[0], [1], [0]]))
                elif b == 's':
                    labels.append(np.array([[0], [0], [1]]))
            elif a == 's':
                if b == 'r':
                    labels.append(np.array([[0], [0], [1]]))
                elif b == 'p':
                    labels.append(np.array([[1], [0], [0]]))
                elif b == 's':
                    labels.append(np.array([[0], [1], [0]]))
        return labels

class CheckpointCallback(tf.keras.callbacks.Callback):
    """Use checkpoint callback to store the best peforming model."""
    def on_epoch_end(self, epoch, logs=None):
        global MIN_VAL_LOSS
        burn_in = 5
        check_point = 1
        if (epoch+1 >= burn_in) and (((epoch+1) % check_point) == 0) and (logs['val_wo_loss'] < MIN_VAL_LOSS):
                MIN_VAL_LOSS = logs['val_wo_loss']
                print("\tSaving model MIN_VAL_LOSS...", end='\r')
                MODEL.save('./model_min_val_loss.model')
                
def load_data(fname):
    """Load the image data."""
    imgs = []
    labels = []
    with open(fname, 'r') as file:
        for l in file:
            img = cv.imread(l.strip())
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            img = img / 255
            imgs.append(img)
            if 'rock' in l:
                labels.append('r')
            elif 'paper' in l:
                labels.append('p')
            elif 'scissors' in l:
                labels.append('s')
    return np.array(imgs), np.array(labels)

def init_nn():
    """Initialize the neural network."""
    def down_block(x, fm_n):
        x = tf.keras.layers.Convolution2D(fm_n, (3, 3), padding='same', activation='relu')(x)
        x = tf.keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)
        return x

    def init_branch(x, branch_out_name):
        """Branch to classify the gesture performed on a certain side of the image."""
        x = down_block(x, 8)
        x = down_block(x, 16)
        x = down_block(x, 32)
        x = tf.keras.layers.Flatten()(x)
        x = tf.keras.layers.Dense(256, activation='relu')(x)
        return tf.keras.layers.Dense(3, activation='softmax', name=branch_out_name)(x)

    def init_decider(left, right):
        """Join branches and decide on winner."""
        x = tf.keras.layers.Concatenate(axis=1)([left, right])
        x = tf.keras.layers.Dense(9, activation='relu')(x)
        return tf.keras.layers.Dense(3, activation='softmax', name='wo')(x)

    input = tf.keras.layers.Input(shape=(40, 120, 1), name='in')
    left_input = tf.keras.layers.Cropping2D(cropping=((0, 0), (0, 60)))(input)
    right_input = tf.keras.layers.Cropping2D(cropping=((0, 0), (60, 0)))(input)
    left = init_branch(left_input, 'lo')
    right = init_branch(right_input, 'ro') 
    out = init_decider(left, right)

    return tf.keras.models.Model(
        inputs=[input],     
        outputs=[left, right, out],
        name='rps_net'
    )


if __name__ == "__main__":
    opt = tf.keras.optimizers.Adadelta(learning_rate=1.0, rho=0.975)
    losses = {
        'lo': 'categorical_crossentropy',
        'ro': 'categorical_crossentropy',
        'wo': 'categorical_crossentropy'
    }
    loss_weights = {
        'lo': 1.0,
        'ro': 1.0,
        'wo': 1.0
    }
    model = init_nn()
    model.summary()
    tf.keras.utils.plot_model(model, to_file='./Output/network.png', show_shapes=True, show_layer_names=True)
    model.compile(optimizer=opt, loss=losses, loss_weights=loss_weights, metrics='accuracy')
    MODEL = model
    
    train_set = load_data('Data/train.txt')
    test_set = load_data('Data/test.txt')
    valid_set = load_data('Data/valid.txt')

    train_data_seq = DataSequence(train_set, BATCH_SIZE)
    val_data_seq = DataSequence(valid_set, BATCH_SIZE)

    es = tf.keras.callbacks.EarlyStopping(monitor='val_wo_loss', patience=10, mode='min')
    history = model.fit(
        train_data_seq,
        validation_data=val_data_seq,
        callbacks=[CheckpointCallback(), es],
        epochs=EPOCHS, batch_size=BATCH_SIZE,
    )

    model.save('./model_final.model')

    plt
    plt.plot(range(len(history.history['wo_loss'])), history.history['wo_loss'], 'r', label='Winner training loss')
    plt.plot(range(len(history.history['val_wo_loss'])), history.history['val_wo_loss'], 'b', label='Winner validation loss')
    plt.plot(range(len(history.history['val_lo_loss'])), history.history['val_lo_loss'], 'g', label='Left validation loss')
    plt.plot(range(len(history.history['val_ro_loss'])), history.history['val_ro_loss'], 'g', label='Right validation loss')
    plt.title('Training and Validation Loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss Value')
    plt.ylim(0, 1)
    plt.legend()
    plt.savefig('./Output/loss.png')

    plt.figure()
    plt.plot(range(len(history.history['wo_accuracy'])), history.history['wo_accuracy'], 'r', label='Winner training accuracy')
    plt.plot(range(len(history.history['val_wo_accuracy'])), history.history['val_wo_accuracy'], 'b', label='Winner validation accuracy')
    plt.title('Training and Validation Accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim(0.9, 1)
    plt.legend()
    plt.savefig('./Output/accuracy.png')